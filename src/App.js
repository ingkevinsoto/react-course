import React from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';
import Dashboard from './components/pages/dashboardPage';
import DetailPage from './components/pages/detailPage';
import StatePage from './components/pages/statePage';
import 'semantic-ui-css/semantic.min.css';

const App =  ({location}) => (

      <div className="ui container">
        <Switch>
          <Route location={location} path='/detail' exact component={DetailPage} />
          <Route location={location} path='/' exact component={Dashboard} />
          <Route location={location} path='/state' exact component={StatePage} />
        </Switch>
      </div>
    
  
);

App.prototype = {
  location: PropTypes.shape({
    pathName: PropTypes.string.isRequired
  }).isRequired
}

export default App;

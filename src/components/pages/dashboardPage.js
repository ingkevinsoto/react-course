import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux'
import { increaseLikes, dismisLikes, reset } from '../../actions/character'
import { Button } from 'semantic-ui-react';

class Dashboard extends React.Component {
    state = {}

    render(){
        return (
            <div>
                <h1>Dashboard Likes {this.props.likes}</h1>
                <Button.Group>
                    <Button onClick={this.props.dismisLikes}
                        content='DisLike'
                        icon='thumbs down'
                        disabled={this.props.likes<1}
                    />
                    <Button.Or text={this.props.likes} />
                    <Button onClick={this.props.increaseLikes}
                        color='red'
                        content='Like'
                        icon='thumbs up'
                    />
                    <Button onClick={this.props.reset}
                        content='Reset'
                        icon='erase'
                    />
                </Button.Group>
                
                <br/>
                <Link to='/detail'>Go to detail page</Link>
                <br/>
                <Link to='/state'>State Page</Link>
                <br />
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    likes : state.character.likes
})

export default connect(mapStateToProps, {increaseLikes, dismisLikes, reset})(Dashboard);
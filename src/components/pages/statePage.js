import React from 'react';
import { Button, Segment } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import { increaseLikes } from '../../actions/character'
import { connect } from 'react-redux'


class StateLocal extends React.Component {
    state = {likes: 0}

    addLike = () => {
        this.setState({
            likes: this.state.likes + 1
        });
    }

    render(){
        const { likes } = this.props;

        return (
            <Segment>
                <h1>Local State</h1>
                <Button onClick={this.props.increaseLikes}
                color='red'
                content='Like'
                icon='heart'
                label={{ basic: true, color: 'red', pointing: 'left', content: likes }}
                />
                <br/>
                <Link to='/'>Go back to Dashboard</Link>
            </Segment>
        );
    }
}

const mapStateToProps = (state) => ({
    likes : state.character.likes
})

export default connect(mapStateToProps, {increaseLikes})(StateLocal);